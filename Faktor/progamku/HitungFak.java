package progamku;
import java.util.Scanner;

public class HitungFak {
    void hitungFaktor(){
        Scanner inp = new Scanner(System.in);
        int nilai = inp.nextInt();
        int faktorial = 1;
            if (nilai <= 0){
                System.out.println("Angka 0");
            } else{
                for(int i=1; i<=nilai; i++){
                    faktorial *=i;
                    System.out.println(String.valueOf(faktorial));
                }
            }
            inp.close();
    }
}
