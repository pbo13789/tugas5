package progamku;
import java.util.Scanner;

public class Array {

    int a [];
    public Array(int index){
        a = new int[index];
    }

    public void setArray(){
        Scanner sc = new Scanner(System.in);
        for (int i=0; i<a.length; i++){
            System.out.print("Set Input Array : ");
            a[i] = sc.nextInt();       
        }
        sc.close();
    }

    void cetak(){
        for (int i = 0; i< a.length; i++){
            System.out.println(a[i] + " ");
        }
    }

    int hitung(){
        int hasil = 1;
        for (int i=0; i<a.length; i++){
            hasil *=a[i];
        }
        System.out.print("Dan hasilnya adalah : ");
        return hasil;
    }
}
